from typing import Tuple
import string

ab = " etaoinsrhldcumfgpywb,.vk-\"_'x)(;0j1q=2:z/*!?$35>{}49[]867\+|&<%@#^`~"
scores = {l: i ** 2 for i, l in enumerate(reversed(ab))}


def string_score(data: bytes) -> int:
    return sum(map(lambda c: scores.get(chr(c).lower(), -1), data))


def single_byte_xor(a: bytes, key: bytes) -> bytes:
    return bytes(c ^ key[0] for c in a)


def cipher(b_in: bytes) -> Tuple[bytes, str, int]:
    data = []
    for i in range(256):
        res = single_byte_xor(b_in, bytes([i]))
        score = string_score(res)
        data.append((score, i, res))

    try:
        best = max(data)
    except ValueError:
        best = (0, 0, b"")

    # (result, key, score)
    return bytes(best[2]), chr(best[1]), best[0]


if __name__ == "__main__":
    hex_str = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    ans, key, score = cipher(bytes.fromhex(hex_str))
    print(key)
    print(single_byte_xor(bytes.fromhex(hex_str), bytes([ord(key)])))
