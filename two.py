def fixed_xor(a: bytes, b: bytes) -> bytes:
    if len(a) != len(b):
        raise ValueError("Both arguments need the same length.")
    return bytes(x ^ y for x, y in zip(a, b))


if __name__ == "__main__":
    a = "1c0111001f010100061a024b53535009181c"
    b = "686974207468652062756c6c277320657965"
    out = fixed_xor(bytes.fromhex(a), bytes.fromhex(b))
    print(out.decode())
