from typing import Tuple

from three import cipher


LineData = Tuple[int, bytes, bytes, str]


def one_from_file(fin: str) -> LineData:
    data = [bytes.fromhex(l.strip()) for l in open(fin)]

    out: LineData = (0, b"", b"", "")
    for line in data:
        ans, key, score = cipher(line)
        if score > out[0]:
            out = (score, ans, line, key)
    return out


if __name__ == "__main__":
    score, res, ln, key = one_from_file("4.txt")
    print(ln, res, key)

