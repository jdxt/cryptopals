import os
from base64 import b64decode
from itertools import count
import string
import random

from eight import is_aes_ecb
from nine import pad_pkcs7
from ten import aes_ecb_encrypt
from twelve import detect_block_size


def byte_ecb_decrypt(data: bytes, prefix_len: int) -> bytes:
    bsize = detect_block_size(data, app, key)
    length = (bsize - prefix_len - (1 + len(data))) % bsize
    crack_len = prefix_len + length + len(data) + 1

    shin = b"A" * length
    short = encrypt(shin)

    for c in range(256):
        val = shin + data + bytes([c])
        enc = encrypt(val)
        if short[:crack_len] == enc[:crack_len]:
            return bytes([c])
    return b""


def encrypt(data: bytes) -> bytes:
    return aes_ecb_encrypt(pad_pkcs7(pre + data + app), key)


def has_eql_block(ctext, bsize):
    for i in range(0, len(ctext) - 1, bsize):
        if ctext[i : i + bsize] == ctext[i + bsize : i + 2 * bsize]:
            return True
    return False


def find_prefix_length(bsize: int):
    ctext1 = encrypt(b"")
    ctext2 = encrypt(b"a")
    prefix_len = 0
    for i in range(0, len(ctext2), bsize):
        if ctext1[i : i + bsize] != ctext2[i : i + bsize]:
            prefix_len = i
            break

    for i in range(bsize):
        fake_in = bytes([0] * (2 * bsize + i))
        ctext = encrypt(fake_in)
        if has_eql_block(ctext, bsize):
            return prefix_len + bsize - i if i != 0 else prefix_len


def byte_ecb_decrypt_hard(data: str) -> bytes:
    bsize = 16
    ctext = encrypt(bytes([0] * 64))
    prefix_len = find_prefix_length(bsize)
    mys_len = len(encrypt(b"")) - prefix_len
    secret_padding = b""
    for i in range(mys_len):
        secret_padding += byte_ecb_decrypt(secret_padding, prefix_len)
    return secret_padding


key = b"\x87\x11\xe6\xa4K\xca\x15\xfbD\xca#5OE\xd0d"
app = b64decode(
    "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg"
    "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq"
    "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg"
    "YnkK"
)
pre = os.urandom(random.randint(1, 255))


if __name__ == "__main__":
    x = byte_ecb_decrypt_hard("x" * 16 * 9)
    print(x.strip(b"\x0b").decode())
