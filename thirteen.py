from typing import Dict, Union
from urllib.parse import unquote

from nine import pad_pkcs7
from ten import aes_ecb_encrypt, aes_ecb_decrypt
from twelve import byte_ecb_decrypt


DataObj = Dict[str, Union[str, int]]


def urlencode(data: DataObj) -> str:
    s = ""
    for k in data:
        s += k + "=" + str(data[k]) + "&"
    return s[:-1]


def parse(data: str) -> DataObj:
    d: DataObj = {}
    for kv in data.split("&"):
        key, v = tuple(map(unquote, kv.split("=")))
        try:
            d[key] = int(v)
        except ValueError:
            d[key] = v
    return d


def profile_for(email: str) -> DataObj:
    return {"email": email.replace("&", "").replace("=", ""), "uid": 10, "role": "user"}


def encrypt_profile(email: str) -> bytes:
    user = profile_for(email)
    return aes_ecb_encrypt(pad_pkcs7(urlencode(user).encode()), key)


def decrypt_profile(data: bytes) -> DataObj:
    dec = aes_ecb_decrypt(data, key).strip(b"\x04").strip(b"\x0b")
    return parse(dec.decode())


def ecb_cut_paste(data: bytes) -> bytes:
    prefix_len = 16 - len("email=")
    suffix_len = 16 - len("admin")
    email1 = "x" * prefix_len + "admin" + (chr(suffix_len) * suffix_len)
    enc1 = encrypt_profile(email1)

    email2 = "master@me.com"
    enc2 = encrypt_profile(email2)

    forced = enc2[:32] + enc1[16:32]
    return forced


key = b"\x05\x91E\xd2\xddW\xb9\x0fZ\xd4\xc3\x91\x94\xd7\x82\xb6"
if __name__ == "__main__":
    x = parse("foo=bar&baz=qux&zap=zazzle")
    # print(x)

    email = "master@me.com"
    enc_user = encrypt_profile(email)
    print("encrypted user profile:")
    print(email)
    print(enc_user)
    print()
    print("ECB cut-pasting result:")
    z = ecb_cut_paste(enc_user)
    user = decrypt_profile(z)
    print(z)
    print(user)
    # assert y == decrypt_profile(enc_user)

