from typing import Tuple, Iterator
from base64 import b64decode

from three import cipher, string_score
from five import rep_key


def to_bits(s: bytes) -> Iterator[int]:
    for c in s:
        bits = bin(c)[2:]
        for b in "00000000"[len(bits) :] + bits:
            yield int(b)


def hamming(a: bytes, b: bytes) -> int:
    if len(a) != len(b):
        raise ValueError("Arguments must have equal length.")
    return sum(x != y for x, y in zip(to_bits(a), to_bits(b)))


def break_rep_key(data: bytes) -> Tuple[bytes, bytes]:
    # get most likely key sizes by looking at closest Hamming chunks
    ks = {}
    for key_size in range(2, 40):
        chunks = [data[i * key_size : i * key_size + key_size] for i in range(4)]
        tot_ham = sum(map(lambda c: hamming(chunks[0], c), chunks[1:]))
        ks[key_size] = tot_ham / key_size

    # test English score of decrypted texts and return highest scoring
    texts = []
    for k in sorted(ks, key=ks.get)[:3]:
        blocks = [data[i * k : i * k + k] for i in range(len(data) // k)]
        t_blocks = (bytes(x) for x in zip(*blocks))

        key = bytes(cipher(blc)[0][0] for blc in t_blocks)
        msg = rep_key(data, key)
        texts.append((string_score(msg.decode()), msg, key))
    return max(texts)[1:]


if __name__ == "__main__":
    print(hamming(b"this is a test", b"wokka wokka!!!"))
    assert hamming(b"this is a test", b"wokka wokka!!!") == 37

    with open("6.txt") as fin:
        data = b64decode(fin.read())

    text, key = break_rep_key(data)
    print("> Decrypted message")
    print("*" * 80)
    print(text.decode())
    print()
    print("> Key")
    print("*" * 80)
    print(key.decode())
