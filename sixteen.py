import os

from ten import aes_cbc_encrypt, aes_cbc_decrypt


def encrypt(sin: str, key: bytes, iv: bytes) -> bytes:
    pre = "comment1=cooking%20MCs;userdata=".encode()
    post = ";comment2=%20like%20a%20pound%20of%20bacon".encode()
    sin = sin.replace(";", "").replace("=", "")
    return aes_cbc_encrypt(pre + sin.encode() + post, key, iv)


def has_admin(data: bytes, key: bytes, iv: bytes) -> bool:
    s = aes_cbc_decrypt(data, key, iv)
    return b";admin=true;" in s


if __name__ == "__main__":
    key = os.urandom(16)
    iv = os.urandom(16)
    x = encrypt("test", key, iv)
    y = has_admin(x, key, iv)
    print(y)
