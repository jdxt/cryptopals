from typing import List
from base64 import b64decode

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from two import fixed_xor
from seven import aes_ecb_decrypt
from nine import unpad


def aes_ecb_encrypt(data: bytes, key: bytes) -> bytes:
    encryptor = Cipher(
        algorithms.AES(key), modes.ECB(), backend=default_backend()
    ).encryptor()
    return encryptor.update(data) + encryptor.finalize()


def aes_cbc_encrypt(data: bytes, key: bytes, iv: bytes):
    blocks = [data[i * 16 : i * 16 + 16] for i in range(len(data) // 16)]
    out = [iv]
    for i, block in enumerate(blocks):
        res = aes_ecb_encrypt(fixed_xor(block, out[i]), key)
        out.append(res)
    return b"".join(out)


def aes_cbc_decrypt(data: bytes, key: bytes, iv: bytes):
    blocks = [iv] if iv else []
    blocks += [data[i * 16 : i * 16 + 16] for i in range(len(data) // 16)]
    out = b""
    for i, block in enumerate(blocks):
        if not i:
            continue
        res = fixed_xor(aes_ecb_decrypt(block, key), blocks[i - 1])
        out += res
    return out


if __name__ == "__main__":
    t_in = b"test" * 32
    t_key = b"yellow submarine"
    assert aes_ecb_decrypt(aes_ecb_encrypt(t_in, t_key), t_key) == t_in

    key = b"YELLOW SUBMARINE"
    iv = b"\x00" * 16
    with open("10.txt", "rb") as fin:
        data = b64decode(fin.read())

    x = aes_cbc_decrypt(data, key, iv)
    print(unpad(x).decode())

