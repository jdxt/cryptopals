def is_aes_ecb(data: bytes) -> bool:
    blocks = [data[i * 16 : i * 16 + 16] for i in range(len(data) // 16)]
    return len(set(blocks)) != len(blocks)


if __name__ == "__main__":

    with open("8.txt", "rb") as fd:
        data = (l.strip() for l in fd.readlines())

    for line in data:
        if is_aes_ecb(line):
            print(line)
