import itertools

from two import fixed_xor


def int_to_byte(c: int) -> bytes:
    h = "0" + hex(c)[2:]
    return bytes.fromhex(h[-2:])


def rep_key(sin: bytes, key: bytes) -> bytes:
    s = b""
    for i, c in enumerate(sin):
        s += fixed_xor(int_to_byte(c), int_to_byte(key[i % len(key)]))
    return s


if __name__ == "__main__":
    phrase = (
        b"Burning 'em, if you ain't quick and nimble\n"
        + b"I go crazy when I hear a cymbal"
    )
    key = b"ICE"

    out = rep_key(phrase, key)
    print(out.hex())

