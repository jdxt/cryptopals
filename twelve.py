import os
from base64 import b64decode
from itertools import count
import string

from eight import is_aes_ecb
from nine import pad_pkcs7
from ten import aes_ecb_encrypt


def detect_block_size(data: bytes, payload: bytes, key: bytes) -> int:
    min_size = len(encrypt(b"", payload, key))
    for i in count(1):
        size = len(encrypt(b"A" * i, payload, key))
        if size != min_size:
            return size - min_size
    return 0


def byte_ecb_decrypt(data: bytes, payload: bytes, key: bytes) -> bytes:
    bsize = detect_block_size(data, payload, key)
    if not is_aes_ecb(data):
        raise ValueError("Input data must be encrypted in ECB mode.")

    chunks = b""
    for block_start in range(0, len(data) - bsize + 1, bsize):
        out = bytearray(bsize)
        for i in range(bsize):
            shin = b"A" * (bsize - 1 - i)
            short = encrypt(shin, payload, key)
            for c in range(256):
                val = shin + chunks + bytes(out[:i]) + bytes([c])
                enc = encrypt(val, payload, key)
                if short[: block_start + bsize] == enc[: block_start + bsize]:
                    out[i] = c
                    break
        chunks += bytes(out)
    return chunks


def encrypt(data: bytes, payload: bytes, key: bytes) -> bytes:
    return aes_ecb_encrypt(pad_pkcs7(data + payload), key)


if __name__ == "__main__":
    key = b"\x87\x11\xe6\xa4K\xca\x15\xfbD\xca#5OE\xd0d"
    app = b64decode(
        """Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
    aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
    dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
    YnkK"""
    )
    x = byte_ecb_decrypt(b"x" * 16 * 9, app, key)
    print(x.strip(b"\x04").decode())
