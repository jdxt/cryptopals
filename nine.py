def unpad(data: bytes) -> bytes:
    return data.strip(b"\x04")


def pad_pkcs7(block: bytes, size: int = 16) -> bytes:
    plen = size - len(block) % size
    return block + b"\x04" * plen


if __name__ == "__main__":
    block = b"YELLOW SUBMARINE"
    x = pad_pkcs7(block, 20)
    print(x)
