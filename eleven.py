import os
import random

from nine import pad_pkcs7
from eight import is_aes_ecb
from ten import aes_cbc_encrypt, aes_ecb_encrypt


def aes_keygen() -> bytes:
    return os.urandom(16)


def encryption_oracle(data: bytes) -> bytes:
    prefix = os.urandom(random.randint(5, 10))
    postfix = os.urandom(random.randint(5, 10))
    data = prefix + data + postfix
    key = aes_keygen()
    if random.randint(0, 1):
        return aes_ecb_encrypt(pad_pkcs7(data, 16), key)
    return aes_cbc_encrypt(data, key, aes_keygen())


if __name__ == "__main__":
    x = encryption_oracle(b"MEANINGLESS JIBBER JABBER " * 10)
    print(x)
