def unpad_pkcs7(data: str):
    if data[-1] != "\x04":
        raise ValueError("String has incorrect padding.")
    return data.strip("\x04")


if __name__ == "__main__":
    x = unpad_pkcs7("ICE ICE BABY\x04\x04\x04\x04")
    print(x)

