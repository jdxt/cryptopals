from base64 import b64decode

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


from nine import unpad


def aes_ecb_decrypt(data: bytes, key: bytes) -> bytes:
    decryptor = Cipher(
        algorithms.AES(key), modes.ECB(), backend=default_backend()
    ).decryptor()
    return decryptor.update(data) + decryptor.finalize()


if __name__ == "__main__":

    with open("7.txt", "rb") as fd:
        data = b64decode(fd.read())
    x = aes_ecb_decrypt(data, b"YELLOW SUBMARINE")
    print(unpad(x).decode())
